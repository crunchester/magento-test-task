<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 01.02.19
 * Time: 16:26
 */

namespace Ivan\HelloWorld\Controller\Index;


use Magento\Framework\App\Action\Action;

class Test extends Action
{

    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
       return $this->_pageFactory->create();
    }

}