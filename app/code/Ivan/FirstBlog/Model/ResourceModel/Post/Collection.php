<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 04.02.19
 * Time: 16:24
 */

namespace Ivan\FirstBlog\Model\ResourceModel\Post;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ivan\FirstBlog\Model\Post', 'Ivan\FirstBlog\Model\ResourceModel\Post');
    }


}