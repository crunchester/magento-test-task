<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 05.02.19
 * Time: 10:19
 */

namespace Ivan\FirstBlog\Model\Post\Source;


class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Ivan\FirstBlog\Model\Post
     */
    protected $post;

    /**
     * Constructor
     *
     * @param \Ivan\FirstBlog\Model\Post $post
     */
    public function __construct(\Ivan\FirstBlog\Model\Post $post)
    {
        $this->post = $post;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->post->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}